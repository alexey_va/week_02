package ru.edu;

public class EmptyTextAnalyzer implements TextAnalyzer {
    /**
     * Класс реализующий сбор статистики.
     */
    private TextStatistics testStatic = new TextStatistics();
    /**
     * Класс реализующий анализ текстовых данных.
     * @param line
     */
    @Override
    public void analyze(final String line) {

        try {
            testStatic.addCharsCount(line.length());
            testStatic.addWordsCount(line.split("[\\s \\n]").length);
            testStatic.addCharsCountWithoutSpaces(
                    line.replaceAll("\\s+", "").length());
            testStatic.addCharsCountOnlyPunctuations(
                    line.split("[\\p{P} ]").length - 1);
            testStatic.countWord(line.trim().split("[\\s^\\w\\.\\,\\-]+"));
        } catch (NullPointerException ex) {
            System.out.println("Передана пустая строка");
        }
    }

    /**
     * Метод предоставления статистики.
     *
     * @return
     */
    @Override
    public TextStatistics getStatistic() {
        return testStatic;
    }
}
