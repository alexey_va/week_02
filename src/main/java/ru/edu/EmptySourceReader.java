package ru.edu;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Класс реализующий работу с исходным файлом.
 */
public class EmptySourceReader implements SourceReader {
    /**
     * Файл источник для анализа.
     */
    private String source;
    /**
     * Класс реализующий сбор статистики.
     */
    TextStatistics textStatic = new TextStatistics();
    //private EmptyTextAnalyzer analyzer = new EmptyTextAnalyzer();

    /**
     * Метод реализует работу с переменной source.
     *
     * @param mySource
     */
    @Override
    public void setup(final String mySource) {
        this.source = mySource;
    }

    /**
     * Класс реализует построчное чтение файла и передачу строки в аналитику.
     * @param analyzer - логика подсчета статистики
     * @return
     * @throws IOException
     */
    @Override
    public TextStatistics readSource(TextAnalyzer analyzer)
            throws IOException {
        try (BufferedReader reader = new
                BufferedReader(new FileReader(this.source))) {
                    while (reader.ready()) {
                        String line = reader.readLine();
                        analyzer.analyze(line);
                    }
        } catch (IOException ex) {
            System.out.println("IOException " + ex);
        }
        return analyzer.getStatistic();
    }
}
