package ru.edu;

import java.util.LinkedHashMap;
import java.util.Comparator;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Необходимо реализовать методы модификации
 * и доступа к хранимым приватным переменным.
 */
public class TextStatistics {

    /**
     * Лимит вывода слов.
     */
    private final int wordLimit = 10;
    /**
     * Переменная сбора рейтинга слов.
     */
    private Map<String, Long> rating = new HashMap<>();
    /**
     * Переменная сбора слов.
     */
    private Map<String, Long> result;
    /**
     * Переменная для сортировки слов.
     */
    private WordSorter wordSorter = new WordSorter();
    /**
     * Всего слов.
     */
    private long wordsCount = 0;
    /**
     * Всего символов.
     */
    private long charsCount = 0;

    /**
     * Всего символов без пробелов.
     */
    private long charsCountWithoutSpaces = 0;

    /**
     * Всего знаков препинания.
     */
    private long charsCountOnlyPunctuations = 0;


    /**
     * Получение количества слов.
     *
     * @return значение
     */
    public long getWordsCount() {
        return wordsCount;
    }

    /**
     * Метод добавления количества слов.
     * @param value
     */
    public void addWordsCount(final long value) {
        wordsCount = wordsCount + value;
    }

    /**
     * Добавление количества символов.
     *
     * @return значение
     */
    public long getCharsCount() {
        return charsCount;
    }

    /**
     * Метод добавления символов.
     * @param value
     */
    public void addCharsCount(final long value) {
        charsCount = charsCount + value;
    }
    /**
     * Получение количества слов без пробелов.
     *
     * @return значение
     */
    public long getCharsCountWithoutSpaces() {
        return charsCountWithoutSpaces;
    }

    /**
     * Метод добавления семволов за исключением пробелов.
     * @param value
     */
    public void addCharsCountWithoutSpaces(final long value) {
        charsCountWithoutSpaces = charsCountWithoutSpaces + value;
    }
    /**
     * Получение количества знаков препинания.
     *
     * @return значение
     */
    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    /**
     * Метод добавления пунктуального символа.
     * @param value
     */
    public void addCharsCountOnlyPunctuations(final long value) {
        charsCountOnlyPunctuations = charsCountOnlyPunctuations + value;
    }
    /**
     * Задание со звездочкой.
     * Необходимо реализовать нахождение топ-10 слов.
     *
     * @return List из 10 популярных слов
     */
    public List<String> getTopWords() {
        Map<String, Integer> sortedMap = wordSorter.getStats();
        List<String> values = sortedMap
                .keySet().stream().limit(wordLimit)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
        return values;
    }

    /**
     * Метод реализует подсчет колличества слов.
     *
     * @param word
     */
    public void countWord(final String[] word) {
        for (String value : word) {
            wordSorter.add(value);
        }
    }


    /**
     * Текстовое представление.
     *
     * @return текст
     */
    @Override
    public String toString() {
        return "TextStatistics{"
                + "wordsCount=" + wordsCount
                + ", charsCount=" + charsCount
                + ", charsCountWithoutSpaces=" + charsCountWithoutSpaces
                + ", charsCountOnlyPunctuations=" + charsCountOnlyPunctuations
                + '}';
    }
    class WordSorter {
        /**
         * Переменная хранит в себе слова и их повторы.
         */
        private final Map<String, Integer> WORDS = new HashMap<>();

        WordSorter() {
        }

        public void add(final String word) {
            WORDS.computeIfPresent(word, (s, i) -> i + 1);
            WORDS.putIfAbsent(word, 1);
        }

        public Map<String, Integer> getStats() {
            return WORDS.entrySet()
                    .stream()
                    .sorted(Collections.reverseOrder(Map.
                            Entry.
                            comparingByValue()))
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (e1, e2) -> e1,
                            LinkedHashMap::new));
        }
    }
}
