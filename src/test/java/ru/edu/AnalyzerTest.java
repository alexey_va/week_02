package ru.edu;

import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.junit.Assert.*;

public class AnalyzerTest {

    /**
     * По умолчанию задачи запускаются с рабочей директорией в корне проекта
     */
    public static final String FILE_PATH = "src/test/resources/input_text.txt";
    public static final int EXPECTED_WORDS = 555;
    public static final int EXPECTED_CHARS = 2724;
    public static final int EXPECTED_CHARS_WO_SPACES = 2210;
    public static final int EXPECTED_CHARS_ONLY_PUNCT = 558;

    /**
     * Нужно переключиться на вашу реализацию
     */
    private TextAnalyzer analyzer = new EmptyTextAnalyzer();

    /**
     * Нужно переключиться на вашу реализацию
     */
    private SourceReader reader = new EmptySourceReader();

    private StatisticReporter reporter = new EmptyStatisticReporter();

    @Test
    public void validation() throws IOException {
        reader.setup(FILE_PATH);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);
        assertEquals(EXPECTED_WORDS, statistics.getWordsCount());
        assertEquals(EXPECTED_CHARS, statistics.getCharsCount());
        assertEquals(EXPECTED_CHARS_WO_SPACES, statistics.getCharsCountWithoutSpaces());
        assertEquals(EXPECTED_CHARS_ONLY_PUNCT, statistics.getCharsCountOnlyPunctuations());
        assertNotNull(statistics);

        List<String> topWords = statistics.getTopWords();

        reporter.report(statistics);
        System.out.println(statistics);
        System.out.println(topWords);

        assertEquals(10, topWords.size());

    }
}