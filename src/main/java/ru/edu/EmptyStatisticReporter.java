package ru.edu;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class EmptyStatisticReporter implements StatisticReporter {
    /**
     * Метод формаирования отчета.
     *
     * @param statistics - данные статистики
     */
    @Override
    public void report(final TextStatistics statistics) {
        File file = new File("./result.txt");

        try (FileWriter fileWriter = new FileWriter(file)) {
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(statistics.toString());
            bufferedWriter.close();
        } catch (IOException ex) {
            System.out.println("IOException " + ex);
        }
    }
}
